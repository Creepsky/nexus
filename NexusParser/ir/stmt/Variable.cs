using Nexus.common;
using Nexus.gen;
using Nexus.ir.expr;

namespace Nexus.ir.stmt
{
    public class Variable : Statement
    {
        public IType Type { get; set; }
        public IExpression Initialization { get; set; }

        public override string ToString()
        {
            return $"{Type} {Name}";
        }

        public override void Check(Context upperContext)
        {
            // TODO
        }

        public override IGenerationElement Generate(Context upperContext, GenerationPhase phase)
        {
            if (upperContext.Element == null)
            {
                throw new NoScopeException(this);
            }

            if (phase == GenerationPhase.Declaration)
            {
                if (upperContext.Element.GetType() == typeof(Class) ||
                    upperContext.Element.GetType() == typeof(Function) ||
                    upperContext.Element.GetType() == typeof(ExtensionFunction) ||
                    upperContext.Element.GetType() == typeof(OperatorFunction))
                {
                    return GenerateParameter(upperContext);
                }

                throw new UnexpectedScopeException(this, upperContext.Element.GetType().Name,
                    new[] {nameof(Class), nameof(Function), nameof(ExtensionFunction) });
            }

            return this;
        }

        public override IType GetResultType(Context context) => Type;

        private IGenerationElement GenerateParameter(Context context)
        {
            context.Add(Name, this);
            return this;
        }

        public override void Print(PrintType type, Printer printer)
        {
            if (type == PrintType.Header)
            {
                Type.Print(type, printer);
                printer.WriteLine($" {Name};");
            }
            else if (type == PrintType.Source)
            {
                printer.Write($"{Name}");
                printer.Write("{");
                Initialization?.Print(type, printer);
                printer.Write("}");
            }
            else if (type == PrintType.Parameter ||
                     type == PrintType.ParameterRef ||
                     type == PrintType.ParameterConstRef)
            {
                Type.Print(type, printer);
                printer.Write(' ' + Name);
            }
            else if (type == PrintType.FunctionSource ||
                     type == PrintType.ForSource)
            {
                Type.Print(type, printer);
                printer.Write(" ");
                printer.Write(Name);
                if (Initialization != null)
                {
                    printer.Write(" = ");
                    Initialization.Print(type, printer);
                }

                if (type == PrintType.ForSource)
                {
                    printer.Write(";");
                }
                else
                {
                    printer.WriteLine(";");
                }
            }
        }
    }
}